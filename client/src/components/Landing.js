import React from "react";
import { Link } from "react-router-dom";

const Landing = () => {
  return (
    <div className="container-fluid d-flex flex-column justify-content-center align-items-center" style={{ height: "100vh" }}>
      <h1 className="display-3 mb-5 text-center">To Do App</h1>
      <p className="lead mb-5 text-center">Stay organized and get things done</p>
      <div className="d-flex justify-content-center">
        <Link to="/login" className="btn btn-primary mr-3 px-5 py-3">
          Login
        </Link>
        <Link to="/register" className="btn btn-secondary ml-3 px-5 py-3">
          Register
        </Link>
      </div>
    </div>
  );
};

export default Landing;
