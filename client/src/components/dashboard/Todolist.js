import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";

import InputTodo from "./todolist/InputTodo";
import ListTodos from "./todolist/ListTodos";
import Cookies from "js-cookie";

const Dashboard = ({ setAuth }) => {
  const [name, setName] = useState("");
  const [allTodos, setAllTodos] = useState([]);
  const [todosChange, setTodosChange] = useState(false);
  const [showGoBackButton, setShowGoBackButton] = useState(false);

  const getProfile = async () => {
    try {
      const res = await fetch("http://localhost:5000/todolist/", {
        method: "GET",
        headers: { jwt_token: Cookies.get("token") }
      });

      const parseData = await res.json();

      setAllTodos(parseData);

      setName(parseData[0].user_name);
    } catch (err) {
      console.error(err.message);
    }
  };

  const logout = async e => {
    e.preventDefault();
    try {
      Cookies.remove("token");
      setAuth(false);
      toast.success("Logout successfully");
    } catch (err) {
      console.error(err.message);
    }
  };

  const handleCheck = async (e) => {
    const isChecked = e.target.value === "true";

    try {
      const response = await fetch(`http://localhost:5000/todolist/userwithcheck/${isChecked}`, {
        method: "GET",
        headers: { jwt_token: Cookies.get("token") }
      });
  
      const data = await response.json();
      console.log(data);
      setAllTodos(data);
      setShowGoBackButton(true);
    } catch (err) {
      console.error(err.message);
    }
  };

  const handleGoBack = () => {
    setShowGoBackButton(false);
    getProfile();
  };

  useEffect(() => {
    getProfile();
    setTodosChange(false);
  }, [todosChange]);

  return (
    <div className="container">
      <div className="d-flex justify-content-between align-items-center py-4">
        <h2 className="mb-0">{name}'s Todo List</h2>
        <button onClick={e => logout(e)} className="btn btn-secondary mt-4">
          Logout
        </button>
      </div>

      <div className="row">
        <div className="col-md-8">
          <InputTodo setTodosChange={setTodosChange} />
        </div>
      </div>

      <div className="row">
      <div className="col">
        <ListTodos allTodos={allTodos} setTodosChange={setTodosChange} />
        <div className="d-flex align-items-center">
          <select className="form-select mx-2" onChange={handleCheck}>
            <option value={false}>Uncompleted</option>
            <option value={true}>Completed</option>
          </select>
          {showGoBackButton && (
            <button
              onClick={handleGoBack}
              className="btn btn-secondary mx-2"
            >
              Go Back
            </button>
          )}
        </div>
      </div>
    </div>
    </div>
  );
};

export default Dashboard;