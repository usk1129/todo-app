import React, { Fragment, useState } from "react";
import Cookies from "js-cookie";

const InputTodo = ({ setTodosChange }) => {
  const [description, setDescription] = useState("");
  
  const onSubmitForm = async e => {
    e.preventDefault();
    try {
      const myHeaders = new Headers();

      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("jwt_token", Cookies.get("token"));

      const body = { description, checked: false };
      const response = await fetch("http://localhost:5000/todolist/todos", {
        method: "POST",
        headers: myHeaders,
        body: JSON.stringify(body)
      });

      const parseResponse = await response.json();

      console.log(parseResponse);

      setTodosChange(true);
      setDescription("");
      // window.location = "/";
    } catch (err) {
      console.error(err.message);
    }
  };


  return (
    <Fragment>
      <h1 className="text-left my-5 ml-30">What's on your to-do list today?</h1>
      <form className="d-flex" onSubmit={onSubmitForm}>
        <input
          type="text"
          placeholder="Add new task"
          className="form-control mr-3"
          value={description}
          onChange={e => setDescription(e.target.value)}
          required
          style={{ width: "70%" }}
        />
        <button className="btn btn-success">Add</button>
      </form>

    </Fragment>
  );
};

export default InputTodo;
