import React, { Fragment, useState, useEffect } from "react";
import EditTodo from "./EditTodo";
import Cookies from "js-cookie";
const ListTodos = ({ allTodos, setTodosChange }) => {

  const [todos, setTodos] = useState([]); 


  async function deleteTodo(id) {
    try {
      await fetch(`http://localhost:5000/todolist/todos/${id}`, {
        method: "DELETE",
        headers: { jwt_token: Cookies.get("token") }
      });

      setTodos(todos.filter(todo => todo.todo_id !== id));
    } catch (err) {
      console.error(err.message);
    }
  }

  async function handleCheck(todo) {
    try {
      const { todo_id, description, checked } = todo;
  
      const body = { description, checked: !checked };
  
      await fetch(`http://localhost:5000/todolist/todos/${todo_id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          jwt_token: Cookies.get("token"),
        },
        body: JSON.stringify(body),
      });
  
      const updatedTodos = todos.map((t) => {
        if (t.todo_id === todo_id) {
          return { ...t, checked: !checked };
        }
        return t;
      });
  
      setTodos(updatedTodos);
    } catch (err) {
      console.error(err.message);
    }
  }

  useEffect(() => {
    setTodos(allTodos);
  }, [allTodos]);

  console.log(todos);

  return (
    <Fragment>
      {" "}
      <table className="table mt-5">
        <thead>
          <tr>
            <th>Description</th>
            <th>Edit</th>
            <th>Completed</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {todos.length !== 0 &&
            todos[0].todo_id !== null &&
            todos.map(todo => (
              <tr key={todo.todo_id}>
                <td>{todo.description}</td>
                <td>
     
                  <EditTodo todo={todo} setTodosChange={setTodosChange} />
                </td>
                <td>
                <input
                    type="checkbox"
                    checked={todo.checked}
                    onChange={() => handleCheck(todo)}
                  />
            
                </td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => deleteTodo(todo.todo_id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </Fragment>
  );
};

export default ListTodos;
