CREATE DATABASE todolist;


--users

CREATE TABLE users
(
  user_id UUID DEFAULT uuid_generate_v4(),
  user_name VARCHAR(255) NOT NULL,
  user_email VARCHAR(255) NOT NULL UNIQUE,
  user_password VARCHAR(255) NOT NULL,
  PRIMARY KEY (user_id)
);

-- run this command if you have not installed it 
-- CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

--todos

CREATE TABLE todos
(
  todo_id SERIAL,
  user_id UUID,
  description VARCHAR(255) NOT NULL,
  checked BOOLEAN DEFAULT false,
  PRIMARY KEY (todo_id),
  FOREIGN KEY (user_id) REFERENCES users(user_id)
);

--seed users data

insert into users
  (user_name, user_email, user_password)
values
  ('seed', 'seed@gmail.com', 'urt2211');

--seed todos data

insert into todos
  (user_id, description,checked)
values
  ('a49dcc60-c210-479f-a561-f68c5bdc5474', 'checking', false);