const express = require("express");
const app = express();
const cors = require("cors");

//middleware
require('dotenv').config();
app.use(cors());
app.use(express.json());

//routes

app.use("/authentication", require("./routes/jwtAuth"));

app.use("/todolist", require("./routes/todolist"));

app.listen(5000, () => {
  console.log(`Server is starting on port 5000`);
});
